const mongoose = require("mongoose");
require("dotenv").config();

const mongodbURI = "mongodb://127.0.0.1:27017/flopachat";

mongoose.set("strictQuery", true);

mongoose.connect(mongodbURI, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

const db = mongoose.connection;

db.on("error", (error) => console.error("Connection error:", error));
db.once("open", () => console.log("Connected to Database"));

module.exports = db;
