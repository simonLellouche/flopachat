
## Configuration du serveur (Express.js)

1. **Initialiser les dépendances :**

    Naviguez dans le dossier `server` et installez les dépendances du projet avec npm.

    ```sh
    cd server
    npm install
    ```

2. **Configurer la base de données :**

    Assurez-vous que MongoDB est en cours d'exécution.

3. **Lancer le serveur :**

    Démarrez le serveur Express.js.

    ```sh
    npm run start
    ```

## Configuration du front-end (Vue.js)

1. **Initialiser les dépendances :**

    Naviguez dans le dossier `front` et installez les dépendances du projet avec npm.

    ```sh
    cd front
    npm install
    ```

2. **Lancer le front-end :**

    Démarrez le serveur de développement Vue.js.

    ```sh
    npm run serve
    ```